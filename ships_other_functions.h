#ifndef SHIPS_OTHER_FUNCTIONS_H
#define SHIPS_OTHER_FUNCTIONS_H

#include <tuple>
#include <string>
#include <algorithm>
#include <queue>
#include <iostream>
//
//
//
#include "ships_struct.h"
#include "ships_define.h"


#include <windows.h>
#include <conio.h>

using namespace std;

extern queue <int> g_ship_to_initiate;

void Clear();

void AddToships_to_initiate(int type, int quantity);

void SetGameSettings();

tuple <bool,int,int> FromS1toS2(string _str);

string FromS2toS1(int _x, int _y);

int stringtoint(string _str);

char stringtochar(string _str);

#endif // SHIPS_OTHER_FUNCTIONS_H
