#ifndef SHIPS_STRUCT_H
#define SHIPS_STRUCT_H

struct SGameSettings
{
    short gamemode;
    short Destroyer_units;
    short Submarine_units;
    short Cruiser_units;
    short Battleship_units;
    short Carrier_units;
};

extern SGameSettings GameSettings;



#endif // SHIPS_STRUCT_H
