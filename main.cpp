#include <iostream>
#include <vector>
#include <queue>
#include <windows.h>
#include <conio.h>
#include <string>
#include <algorithm>
#include <tuple>
#include <stdlib.h>
#include <ctime>
#include <fstream>
#include <sstream>
#include <iterator>
//
#include "ships_game.h"

HANDLE hOut;
queue <int> g_ship_to_initiate;
char g_turn;
SGameSettings GameSettings;

//NAMESPACE
using namespace std;

int main()
{
    srand(time(NULL));
    //
    g_turn = 0;
    MENU();
    //
    if(GameSettings.gamemode == 0)
    {
        SetGameSettings();
        Clear();
        cBoard Player1(0,"A");
        Player1.SetShipInitial();
        //
        Clear();
        cBoard Player2(1,"B");
        Player2.SetShipInitial();
        //
        PLAY(Player1,Player2);
    }
    if(GameSettings.gamemode == 1)
    {
        SetGameSettings();
        Clear();
        cBoard Player1(0,"A");
        Player1.SetShipInitial();
        //
        SetGameSettings();
        Clear();
        cBoard Player2(0,"B");
        Player2.SetShipInitial();
        //
        PLAY(Player1,Player2);
    }
    return 0;
}
