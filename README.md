### Funkcjonalnosc ###

![This is an image](https://cdn.discordapp.com/attachments/918171716436500480/981308135002222662/1.PNG)

Za pomocą klawiatury numerycznej decydujemy czy chcemy rozpocząc grę, dostosować ustawienia lub wybrać tryb gry (Gracz vs Gracz/Gracz vs Komputer). <br />
- W ustawieniach możemy decydować o ilości statków ustawianych na planszy, <br />

### Narzedzia ###
Do wykonania naszego projektu uzyliśmy:
* QTCreator 6.0.2(community), <br />
* Codeblocks, <br />
* Git - system kontroli wersji, <br />
* BitBucket - wykorzystujący Git'a. <br />

### Przebieg projektu ###

Każdy z programistów pracował samodzielnie nad wyznaczonymi wcześniej zadaniami, natomiast w razie 
problemów lub aby zaczerpnąć rady co jakiś czas spotykalismy się na Discordzie. Swoje części kodu
jak i już skończoną pracę testowaliśmy razem.

### Podział ###

Podział zadań:

- Jan Czaja - koordynacja projektem, oprawa logiczna, <br />
- Dawid Nosal - statki, wygląd menu, <br />
- Natalia Martemianow - ogólne rysowanie planszy. <br />