#ifndef SHIPS_GAME_H
#define SHIPS_GAME_H

#include "cboard.h"

// ZMIENNE POJEDYNCZE - GLOBALNE
extern char g_turn; // 0 - start , 1 - gracz 1. , 2 - gracz 2.

void PLAY(cBoard Player1, cBoard Player2);

void LogoDraw();

void MENU();

#endif // SHIPS_GAME_H
