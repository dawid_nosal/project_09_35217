#ifndef CBOARD_H
#define CBOARD_H

#include <iostream>
#include <vector>
#include <queue>
#include <windows.h>
#include <conio.h>
#include <string>
#include <algorithm>
#include <tuple>
#include <stdlib.h>
#include <ctime>
#include <fstream>
#include <sstream>
#include <iterator>
//
#include "cShip.h"
#include "ships_define.h"
#include "ships_other_functions.h"

using namespace std;
extern queue <int> g_ship_to_initiate;
extern HANDLE hOut;

class cBoard
{
private:
    E_Board_field board_my [BOARD_SIZE][BOARD_SIZE];
    E_Board_field board_enemy [BOARD_SIZE][BOARD_SIZE];
    vector <cShip> vships;
    bool flag_ship_seted;
    bool BOOT;
    string name;
    friend void PLAY(cBoard Player1, cBoard Player2);
    //
    struct
    {
        short position_x;
        short position_y;
        E_Direction direction;
    }ShipInfo;
    //
public:
    cBoard(bool _BOOT, string _name);

    ~cBoard();

    void DebugInfo();

    void SetShipOnBoard(cShip &obj);

    void RemoveShip();

    void SetShipOnBoardENEMY(cShip &obj);

    int AttackOnOpponent(int _xpos, int _ypos, cBoard * opponent);

    void DrawDebugBoard();

    bool CanBeSet(cShip &obj);

    void GetShipInfoFromUser(short _size);

    void SetNewShip();

    bool InitShipFromFile();

    void SetShipInitial();

    void BootAI(cBoard * opponent);
};

#endif // CBOARD_H
