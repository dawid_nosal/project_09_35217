TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        cboard.cpp \
        cship.cpp \
        main.cpp \
        ships_game.cpp \
        ships_other_functions.cpp

HEADERS += \
    cboard.h \
    cship.h \
    ships_define.h \
    ships_enums.h \
    ships_game.h \
    ships_other_functions.h \
    ships_struct.h
