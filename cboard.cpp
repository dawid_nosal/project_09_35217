#include "cboard.h"

cBoard::cBoard(bool _BOOT, string _name)
{
    for(int i = 0 ; i < BOARD_SIZE ; i++)
    {
        for(int j = 0; j < BOARD_SIZE ; j++)
        {
            board_my[i][j] = E_Board_field::EMPTY;
            board_enemy[i][j] = E_Board_field::EMPTY;
        }
    }
    //
    BOOT = _BOOT;
    name = _name;
    flag_ship_seted = 0;
}

cBoard::~cBoard()
{

}

void cBoard::DebugInfo()
{
    cout << "vships" << endl;
    //
    cout << "vships - size: " << vships.size() << endl;
    for(int i = 0; i < (int)(vships.size()); i++)
    {
        cout << i << ".     " << endl;
        cout << vships[i].position_x << "   " << vships[i].position_y  << "   " << vships[i].size << "   " << vships[i].direction << endl;
    }
    //
    getch();
}

void cBoard::SetShipOnBoard(cShip &obj)
{
    switch(obj.direction)
    {
    case NORTH:
        for(int i = (obj.position_x-1) ; i <= (obj.position_x+1) ; i++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y-1) ; j <= (obj.position_y+obj.size) ; j++)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_my[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_y ; k <= (obj.position_y+(obj.size-1)) ; k++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_my[obj.position_x][k] = E_Board_field::SHIP_ALIVE;
        }
        break;
        //
    case EAST:
        for(int i = (obj.position_x-1) ; i <= (obj.position_x+obj.size) ; i++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y-1) ; j <= (obj.position_y+1) ; j++)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_my[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_x ; k <= (obj.position_x+(obj.size-1)) ; k++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_my[k][obj.position_y] = E_Board_field::SHIP_ALIVE;
        }
        break;
        //
    case SOUTH:
        for(int i = (obj.position_x-1) ; i <= (obj.position_x+1) ; i++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y+1) ; j >= (obj.position_y-obj.size) ; j--)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_my[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_y ; k >= (obj.position_y-(obj.size-1)) ; k--)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_my[obj.position_x][k] = E_Board_field::SHIP_ALIVE;
        }
        break;
        //
    case WEST:
        for(int i = (obj.position_x+1) ; i >= (obj.position_x-obj.size) ; i--)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y-1) ; j <= (obj.position_y+1) ; j++)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_my[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_x ; k >= (obj.position_x-(obj.size-1)) ; k--)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_my[k][obj.position_y] = E_Board_field::SHIP_ALIVE;
        }
        break;
        //
    }
}

void cBoard::RemoveShip() //OK
{
    cout << endl;
    cout << "Ktory statek usunac? :" << endl;
    //
    if(vships.empty())
    {
        cout << "Na planszy nie ma statku do usuniecia" << endl;
    }
    //
    for(int i = 0; i < (int)vships.size() ; i++)
    {
        cout << i+1 << ". ";
        cout << "Od pozycji: ";
        cout << FromS2toS1(vships[i].position_x,vships[i].position_y);
        cout << " do pozycji ";
        switch(vships[i].direction)
        {
        case NORTH:
            cout << FromS2toS1(vships[i].position_x,vships[i].position_y+(vships[i].size-1));
            break;
            //
        case EAST:
            cout << FromS2toS1(vships[i].position_x+(vships[i].size-1), vships[i].position_y);
            break;
            //
        case SOUTH:
            cout << FromS2toS1(vships[i].position_x, vships[i].position_y-(vships[i].size-1));
            break;
            //
        case WEST:
            cout << FromS2toS1(vships[i].position_x-(vships[i].size-1), vships[i].position_y);
            break;
            //
        }
        cout << endl << endl;
    }
    //
    bool while1 = 1;
    int ans_int;
    while(while1)
    {
        cout << "---> ";
        string ans;
        cin >> ans;
        ans_int = stringtoint(ans);
        if(ans_int > 0)
        {
            while1 = 0;
        }
        ans_int--;
    }
    //
    g_ship_to_initiate.push(vships[ans_int].size);
    vships.erase(vships.begin()+(ans_int));
    //
    for(int i = 0 ; i < BOARD_SIZE ; i++)
    {
        for(int j = 0; j < BOARD_SIZE ; j++)
        {
            board_my[i][j] = E_Board_field::EMPTY;
        }
    }
    for(int i = 0; i < (int)vships.size();i++)
    {
        SetShipOnBoard(vships[i]);
    }

}

void cBoard::SetShipOnBoardENEMY(cShip &obj) //OK
{
    switch(obj.direction)
    {
    case NORTH:
        for(int i = (obj.position_x-1) ; i <= (obj.position_x+1) ; i++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y-1) ; j <= (obj.position_y+obj.size) ; j++)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_enemy[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_y ; k <= (obj.position_y+(obj.size-1)) ; k++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_enemy[obj.position_x][k] = E_Board_field::SHIP_DESTROY;
        }
        break;
        //
    case EAST:
        for(int i = (obj.position_x-1) ; i <= (obj.position_x+obj.size) ; i++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y-1) ; j <= (obj.position_y+1) ; j++)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_enemy[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_x ; k <= (obj.position_x+(obj.size-1)) ; k++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_enemy[k][obj.position_y] = E_Board_field::SHIP_DESTROY;
        }
        break;
        //
    case SOUTH:
        for(int i = (obj.position_x-1) ; i <= (obj.position_x+1) ; i++)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y+1) ; j >= (obj.position_y-obj.size) ; j--)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_enemy[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_y ; k >= (obj.position_y-(obj.size-1)) ; k--)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_enemy[obj.position_x][k] = E_Board_field::SHIP_DESTROY;
        }
        break;
        //
    case WEST:
        for(int i = (obj.position_x+1) ; i >= (obj.position_x-obj.size) ; i--)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 1" << endl;
            for(int j = (obj.position_y-1) ; j <= (obj.position_y+1) ; j++)
            {
                if(i >= 0  && i < BOARD_SIZE && j >= 0 && j < BOARD_SIZE) board_enemy[i][j] = E_Board_field::WATER_BY_SHIP;
                if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 2" << endl;
            }
        }
        for(int k = obj.position_x ; k >= (obj.position_x-(obj.size-1)) ; k--)
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetShipOnBoard: IN FOR 3" << endl;
            if(k >= 0  && k < BOARD_SIZE ) board_enemy[k][obj.position_y] = E_Board_field::SHIP_DESTROY;
        }
        break;
        //
    }
}

int cBoard::AttackOnOpponent(int _xpos, int _ypos, cBoard * opponent) //OK
{
    int size  = (int)(opponent->vships.size());
    if(CONSOLE_DEBUG) cout << "FUNKTION - AttackOnOpponent: " << "size: " << size <<  endl;
    //
    for(int i = 0; i < size; i++)
    {
        int ans = opponent->vships[i].Ship_hited(_xpos,_ypos);
        switch(ans)
        {
        case 0:
            board_enemy[_xpos][_ypos] = E_Board_field::MISS;
            break;
            //
        case 1:
            board_enemy[_xpos][_ypos] = E_Board_field::HIT;
            opponent->board_my[_xpos][_ypos] = E_Board_field::SHIP_DESTROY;
            return 1;
            break;
            //
        case 2:
            board_enemy[_xpos][_ypos] = E_Board_field::HIT;
            opponent->board_my[_xpos][_ypos] = E_Board_field::SHIP_DESTROY;
            //
            SetShipOnBoardENEMY(opponent->vships[i]);
            return 2;
            break;
            //
        }
    }
    return 0;
    //
}

void cBoard::DrawDebugBoard() //OK
{
    int k=1;
    cout << "----------------------------------------------------------------------------------------------------" << endl;
    //
    cout << "     ";
    for(char c = 'A'; c <= 'J'; c++)
    {
        cout << c << "  ";
    }

    cout << "\t\t";
    cout << "     ";
    for(char c = 'A'; c <= 'J'; c++)
    {
        cout << c << "  ";
    }
    cout << endl << endl;

    for(int i = (BOARD_SIZE-1) ; i >= 0 ; i--)
    {
        for(int j = 0 ; j < BOARD_SIZE ; j++)
        {
            if(j==0)
            {
                if(k<BOARD_SIZE)
                {
                    cout << " ";
                }
                cout << k << "  ";
            }

            hOut = GetStdHandle( STD_OUTPUT_HANDLE );
            SetConsoleTextAttribute( hOut, 11 );

            switch(board_my[j][i])
            {
            case 0:
                SetConsoleTextAttribute( hOut, 63);//Tlo
                cout << " " << '~' << " ";
                break;
            case 1:
                SetConsoleTextAttribute( hOut, 47);
                cout << " " << '#' << " ";
                break;
            case 2:
                SetConsoleTextAttribute( hOut, 79);
                cout << " " << 'X' << " ";
                break;
            case 3:
                SetConsoleTextAttribute( hOut, 63);//Teren wylaczony po zatopieniu i ustawieniu
                cout << " " << '~' << " ";
                break;
            case 6:
                SetConsoleTextAttribute( hOut, 31);//Pudlo
                cout << " " << '~' << " ";
                break;
            default:
                SetConsoleTextAttribute( hOut, 63);//Tlo
                cout << " " << '~' << " ";
                break;
            }
        }
        cout << "\t\t";

        for(int j = 0 ; j < BOARD_SIZE ; j++)
        {
            if(j==0)
            {
                SetConsoleTextAttribute( hOut, 15);
                if(k<BOARD_SIZE)
                {
                    cout << " ";
                }
                cout << k << "  ";
            }

            hOut = GetStdHandle( STD_OUTPUT_HANDLE );
            SetConsoleTextAttribute( hOut, 11 );

            switch(board_enemy[j][i])
            {
            case 0:
                SetConsoleTextAttribute( hOut, 63);//Tlo
                cout << " " << '~' << " ";
                break;
            case 2:
                SetConsoleTextAttribute( hOut, 207);//Statek zniszczony
                cout << " " << '#' << " ";
                break;
            case 3:
                SetConsoleTextAttribute( hOut, 31);//Teren wylaczony po zatopieniu i ustawieniu
                cout << " " << '~' << " ";
                break;
            case 4:
                SetConsoleTextAttribute( hOut, 79);//Trafony
                cout << " " << 'X' << " ";
                break;
            case 5:
                SetConsoleTextAttribute( hOut, 159);//Pudlo
                cout << " " << '~' << " ";
                break;
            default:
                SetConsoleTextAttribute( hOut, 63);//Tlo
                cout << " " << '~' << " ";
                break;
            }
        }
        k++;
        cout << endl ;
        SetConsoleTextAttribute( hOut, 15);//Default

    }
}

bool cBoard::CanBeSet(cShip &obj) //OK
{
    if(CONSOLE_DEBUG) cout << "FUN - CanBeSet:" << "  Pos_x: " << obj.position_x << "   Pos_y: " << obj.position_y <<   "Size: " << obj.size << "   DIR: " << obj.direction <<endl;
    //
    bool flag_CanBeSet = 1;
    //
    if(!(obj.position_x >= 0 && obj.position_x < BOARD_SIZE)) // Czy punkt odniesienia statku znajduje się w granicach planszy (oś X)
    {
        if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 1" << endl;
        flag_CanBeSet = 0;
    }
    //
    if(!(obj.position_y >= 0 && obj.position_y < BOARD_SIZE)) // Czy punkt odniesienia statku znajduje się w granicach planszy (oś Y)
    {
        if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 2" << endl;
        flag_CanBeSet = 0;
    }
    //
    switch(obj.direction)
    {
    case NORTH:
        if(obj.position_y + (obj.size-1) > (BOARD_SIZE-1))
        {
            if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 3.1" << endl;
            flag_CanBeSet = 0;
        }
        break;
        //
    case EAST:
        if(obj.position_x + (obj.size-1) > (BOARD_SIZE-1))
        {
            if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 3.2" << endl;
            flag_CanBeSet = 0;
        }
        break;
        //
    case SOUTH:
        if(obj.position_y - (obj.size-1) < 0)
        {
            if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 3.3" << endl;
            flag_CanBeSet = 0;
        }
        break;
        //
    case WEST:
        if(obj.position_x - (obj.size-1) < 0)
        {
            if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 3.4" << endl;
            flag_CanBeSet = 0;
        }
        break;
        //
    }
    //
    //
    //
    switch(obj.direction)
    {
    case NORTH:
        for(int i = obj.position_y ; i <= (obj.position_y + (obj.size-1)) ; i++)
        {
            if(!(board_my[obj.position_x][i] == E_Board_field::EMPTY))
            {
                if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 4.1" << endl;
                flag_CanBeSet = 0;
            }
        }
        break;
        //
    case EAST:
        for(int i = obj.position_x ; i <= (obj.position_x + (obj.size-1)) ; i++)
        {
            if(!(board_my[i][obj.position_y] == E_Board_field::EMPTY))
            {
                if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 4.2" << endl;
                flag_CanBeSet = 0;
            }
        }
        break;
        //
    case SOUTH:
        for(int i = obj.position_y ; i >= (obj.position_y - (obj.size-1)) ; i--)
        {
            if(!(board_my[obj.position_x][i] == E_Board_field::EMPTY))
            {
                if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 4.3" << endl;
                flag_CanBeSet = 0;
            }
        }
        break;
        //
    case WEST:
        for(int i = obj.position_x ; i >= (obj.position_x - (obj.size-1)) ; i--)
        {
            if(!(board_my[i][obj.position_y] == E_Board_field::EMPTY))
            {
                if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: EXP 4.4" << endl;
                flag_CanBeSet = 0;
            }
        }
        break;
        //
    }
    //
    if(CONSOLE_DEBUG) cout << "FUN - CanBeSet: flag_CanBeSet: " << flag_CanBeSet << endl;
    return flag_CanBeSet;
}

void cBoard::GetShipInfoFromUser(short _size) //OK
{
    cout << "Wielkosc statku: " << _size << endl;
    //
    while(1)
    {
        cout << "Podaj punkt poczatkowy statku: ";
        //
        string point;
        cin >> point;
        tuple <bool,int,int> ans;
        ans = FromS1toS2(point);
        if(get<0>(ans) == 1)
        {
            ShipInfo.position_x = get<1>(ans);
            ShipInfo.position_y = get<2>(ans);
            break;
        }
    }
    //
    cout << endl;
    //
    bool while2_2 = 1;
    while(while2_2)
    {
        if(_size != 1)
        {
            cout << "Podaj kierunek obrotu: (N,E,S,W): ";
            string dir;
            cin >> dir;
            char dir_char = stringtochar(dir);
            //
            switch(dir_char)
            {
            case 'N':
            case 'n':
            case '1':
                ShipInfo.direction = E_Direction::NORTH;
                while2_2 = 0;
                break;
                //
            case 'E':
            case 'e':
            case '2':
                ShipInfo.direction = E_Direction::EAST;
                while2_2 = 0;
                break;
                //
            case 'S':
            case 's':
            case '3':
                ShipInfo.direction = E_Direction::SOUTH;
                while2_2 = 0;
                break;
                //
            case 'W':
            case 'w':
            case '4':
                ShipInfo.direction = E_Direction::WEST;
                while2_2 = 0;
                break;
            default:
                if(CONSOLE_DEBUG) cout << "FUN - GetShipInfoFromUser:" << endl;
                if(CONSOLE_DEBUG) cout << "PODANO: " << dir_char << endl;
                break;
            }
        }
        else
        {
            ShipInfo.direction = E_Direction::NORTH;
            while2_2 = 0;
        }
    }
}

void cBoard::SetNewShip() //OK
{
    if(g_ship_to_initiate.empty())
    {
        cout << "BRAK STATKOW DO POSTAWIENIA" << endl;
        getch();
        return;
    }
    //
    short size = g_ship_to_initiate.front();
    //
    while(1)
    {
        GetShipInfoFromUser(size);
        //
        cShip provisional_obj(size,ShipInfo.position_x,ShipInfo.position_y,ShipInfo.direction);
        //
        if(CanBeSet(provisional_obj))
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetNewShip: IN IF" << endl;
            SetShipOnBoard(provisional_obj);
            vships.push_back(provisional_obj);
            g_ship_to_initiate.pop();
            //DrawDebugBoard();
            break;
        }
        else
        {
            if(CONSOLE_DEBUG) cout << "FUN - SetNewShip: EXP 1" << endl;
            cout << "Nie mozna postawic statku w takim miejscu." << endl;
            cout << endl;
        }
    }
}

bool cBoard::InitShipFromFile() //OK
{
    fstream file;
    int numer_pliku = 1;
    string plikdootwarcia = "";
    int ilosc_plikow = 0;
    ///
    while(1) //Sprawdzanie ile istnieje plikow do wyboru
    {
        plikdootwarcia = "";
        plikdootwarcia += "set_";
        plikdootwarcia += to_string(numer_pliku);
        plikdootwarcia += ".txt";
        //
        file.open(plikdootwarcia,ios::in);
        if(file.good())
        {
            numer_pliku++;
            file.close();
        }
        else
        {
            ilosc_plikow = numer_pliku;
            file.close();
            break;
        }
    }
    //
    while(1) // Losowanie pliku z puli
    {
        numer_pliku = rand()%ilosc_plikow + 1;
        //
        plikdootwarcia = "";
        plikdootwarcia += "set_";
        plikdootwarcia += to_string(numer_pliku);
        plikdootwarcia += ".txt";
        //
        file.open(plikdootwarcia,ios::in);
        if(file.good())
        {
            break;
        }
        else
        {
            file.close();
        }
    }
    //
    string komenda;
    // int nr_komendy = 1;
    string fpos,fdir,fsize;
    while(getline(file,komenda))
    {
        //cout << komenda << endl;
        //
        stringstream ss(komenda);
        string s;
        short nr_op = 0;
        while(getline(ss,s,' '))
        {
            switch(nr_op)
            {
            case 0:
                fpos = s;
                break;
                //
            case 1:
                fdir = s;
                break;
                //
            case 2:
                fsize = s;
                break;
                //
            }
            //
            nr_op++;
        }
        // cout << komenda << "\t" << fpos << "\t" << fdir << "\t" << fsize << endl;
        tuple <bool,int,int> coordinates = FromS1toS2(fpos);
        E_Direction dir;
        switch(stringtochar(fdir))
        {
        case 'N': dir = E_Direction::NORTH; break;
        case 'E': dir = E_Direction::EAST; break;
        case 'S': dir = E_Direction::SOUTH; break;
        case 'W': dir = E_Direction::WEST; break;
        }
        short size = stoi(fsize);
        //
        cShip provisional_obj(size,get<1>(coordinates),get<2>(coordinates),dir);
        //
        if(CanBeSet(provisional_obj))
        {
            SetShipOnBoard(provisional_obj);
            vships.push_back(provisional_obj);
        }
    }
    //
    file.close();
    return true;
}

void cBoard::SetShipInitial() //OK
{
    if(BOOT)
    {
        InitShipFromFile();
    }
    else
    {
        bool flag_SetShipInitial_while = 1;
        while(flag_SetShipInitial_while)
        {
            Clear();
            //
            cout << "GRACZ: " << name << endl;
            //
            DrawDebugBoard();
            //
            cout << endl;
            cout << "Co zrobic? :" << endl;
            cout << "1.Nowy statek. " << endl;
            cout << "2.Zdejmij statek. " << endl;
            cout << "3.Zakoncz ustawianie. " << endl;
            if(CONSOLE_DEBUG) cout << "4.DEBUG INFO " << endl;
            cout << "---> ";
            //
            string ans;
            cin >> ans;
            int ans_int = stringtoint(ans);
            //
            switch(ans_int)
            {
            case 1:
                SetNewShip();
                break;
                //
            case 2:
                RemoveShip();
                break;
                //
            case 3:
                if(g_ship_to_initiate.empty()) flag_SetShipInitial_while=0;
                else
                {
                    cout << "Nie wszystkie statki ustawione" << endl;
                    getch();
                }
                break;
                //
            case 4:
                if(CONSOLE_DEBUG) DebugInfo();
                break;
                //
            default:
                cout << "Nie poprawna opcja;" << endl;
                getch();
                break;
            }
        }
    }

}

void cBoard::BootAI(cBoard * opponent) //OK
{
    if(CONSOLE_DEBUG) cout << "BOOT" << endl;
    if(CONSOLE_DEBUG) getch();
    //Inicjalizacja zmiennych statycznych
    static bool ship_find = 0;
    static bool found_dir = 0;
    static short x_first =0, y_first =0;
    static short x_last_attac = 0, y_last_attac = 0;
    static short x_to_attac = 0, y_to_attac = 0;
    static short x_rand = 0, y_rand = 0;
    static E_Direction dir = INDEFINITE;
    static short s_dir = 0;
    int ans = -1;
    static short attaced_ship_size = 0;
    //
    //
    //
    if(ship_find == 0 && found_dir == 0) // POSZUKIWANIE STATKU
    {
        while(1)
        { // Losowanie pola PUSTEGO
            x_rand = (short)rand()%10;
            y_rand = (short)rand()%10;
            if(board_enemy[x_rand][y_rand] == E_Board_field::EMPTY)
            {
                x_first = x_rand;
                x_to_attac = x_rand;
                y_first = y_rand;
                y_to_attac = y_rand;
                break;
            }
        }
        //
        ans = AttackOnOpponent(x_to_attac,y_to_attac,opponent);
        //
        switch(ans)
        {
        case 0: // Pudlo
            if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF1 - CASE 0: " << x_to_attac << " " << y_to_attac << endl;
            if(CONSOLE_DEBUG) getch();
            board_enemy[x_to_attac][y_to_attac] = E_Board_field::MISS;
            opponent->board_my[x_to_attac][y_to_attac] = E_Board_field::ATTACED_BY_ENEMY;
            return;
            break;
            //
        case 1: // Statek znaleziony
            if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF1 - CASE 1: " << x_to_attac << " " << y_to_attac << endl;
            if(CONSOLE_DEBUG) getch();
            board_enemy[x_to_attac][y_to_attac] = E_Board_field::HIT;
            attaced_ship_size++;
            ship_find = 1;
            //
            x_last_attac = x_to_attac;
            y_last_attac = y_to_attac;
            //
            s_dir = 0;
            dir = E_Direction::INDEFINITE;
            return;
            break;
            //
        case 2:
            if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF1 - CASE 2: " << x_to_attac << " " << y_to_attac << endl;
            if(CONSOLE_DEBUG) getch();
            attaced_ship_size++;
        {
            cShip provisional_obj(1,x_to_attac,y_to_attac,NORTH);
            SetShipOnBoardENEMY(provisional_obj);
        }
            attaced_ship_size=0;
            s_dir = 0;
            dir = E_Direction::INDEFINITE;
            return;
            break;
            //
        }
        return;
    }
    //
    //
    //
    if(ship_find == 1 && found_dir == 0) // STATEK ZNALEZIONY _ SZUKANIE KIERUNKU OBROTU
    {
        bool prubny_strzal_oddany = 0;
        while(prubny_strzal_oddany != 1)
        {
            switch(s_dir)
            {
            case 0:
                x_to_attac = x_first;
                y_to_attac = y_first + 1;
                if(x_to_attac >= 0 && x_to_attac < BOARD_SIZE && y_to_attac >=0 && y_to_attac < BOARD_SIZE)
                {
                    if(board_enemy[x_to_attac][y_to_attac] == E_Board_field::EMPTY)
                    {
                        ans = AttackOnOpponent(x_to_attac,y_to_attac,opponent);
                        prubny_strzal_oddany = 1;
                    }
                    else
                    {
                        s_dir = 1;
                    }
                }
                else
                {
                    s_dir = 1;
                }
                break;
                //
            case 1:
                x_to_attac = x_first + 1;
                y_to_attac = y_first;
                if(x_to_attac >= 0 && x_to_attac < BOARD_SIZE && y_to_attac >=0 && y_to_attac < BOARD_SIZE)
                {
                    if(board_enemy[x_to_attac][y_to_attac] == E_Board_field::EMPTY)
                    {
                        ans = AttackOnOpponent(x_to_attac,y_to_attac,opponent);
                        prubny_strzal_oddany = 1;
                    }
                    else
                    {
                        s_dir = 2;
                    }
                }
                else
                {
                    s_dir = 2;
                }
                break;
                //
            case 2:
                x_to_attac = x_first;
                y_to_attac = y_first - 1;
                if(x_to_attac >= 0 && x_to_attac < BOARD_SIZE && y_to_attac >=0 && y_to_attac < BOARD_SIZE)
                {
                    if(board_enemy[x_to_attac][y_to_attac] == E_Board_field::EMPTY)
                    {
                        ans = AttackOnOpponent(x_to_attac,y_to_attac,opponent);
                        prubny_strzal_oddany = 1;
                    }
                    else
                    {
                        s_dir = 3;
                    }
                }
                else
                {
                    s_dir = 3;
                }
                break;
                //
            case 3:
                x_to_attac = x_first - 1;
                y_to_attac = y_first;
                if(x_to_attac >= 0 && x_to_attac < BOARD_SIZE && y_to_attac >=0 && y_to_attac < BOARD_SIZE)
                {
                    if(board_enemy[x_to_attac][y_to_attac] == E_Board_field::EMPTY)
                    {
                        ans = AttackOnOpponent(x_to_attac,y_to_attac,opponent);
                        prubny_strzal_oddany = 1;
                    }
                    else
                    {
                        s_dir = 0;
                    }
                }
                else
                {
                    s_dir = 0;
                }
                break;
                //
            }
        }
        // Prubny strzał oddany
        switch(ans)
        {
        case 0:
            if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF2 - CASE 0: " << x_to_attac << " " << y_to_attac << endl;
            if(CONSOLE_DEBUG) getch();
            s_dir++; // Skoro nie trafiony obruć się przed następnym
            board_enemy[x_to_attac][y_to_attac] = E_Board_field::MISS;
            opponent->board_my[x_to_attac][y_to_attac] = E_Board_field::ATTACED_BY_ENEMY;
            return;
            break;
            //
        case 1:
            if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF2 - CASE 1: " << x_to_attac << " " << y_to_attac << endl;
            if(CONSOLE_DEBUG) getch();
            found_dir = 1;
            //
            switch(s_dir)
            {
            case 0: dir = E_Direction::NORTH; break;
            case 1: dir = E_Direction::EAST; break;
            case 2: dir = E_Direction::SOUTH; break;
            case 3: dir = E_Direction::WEST; break;
            }
            x_last_attac = x_to_attac;
            y_last_attac = y_to_attac;
            attaced_ship_size++;
            board_enemy[x_to_attac][y_to_attac] = E_Board_field::HIT;
            return;
            break;
            //
        case 2:
            if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF2 - CASE 2: " << x_to_attac << " " << y_to_attac << endl;
            if(CONSOLE_DEBUG) getch();
            attaced_ship_size++;
            switch(s_dir)
            {
            case 0: dir = E_Direction::NORTH; break;
            case 1: dir = E_Direction::EAST; break;
            case 2: dir = E_Direction::SOUTH; break;
            case 3: dir = E_Direction::WEST; break;
            }
            switch(dir)
            {
            case NORTH: dir = E_Direction::SOUTH; break;
            case EAST: dir = E_Direction::WEST; break;
            case SOUTH: dir = E_Direction::NORTH; break;
            case WEST: dir = E_Direction::EAST; break;
            }
        {
            cShip provisional_obj(attaced_ship_size,x_to_attac,y_to_attac,dir);
            SetShipOnBoardENEMY(provisional_obj);
        }
            attaced_ship_size=0;
            //
            ship_find = 0;
            found_dir = 0;
            s_dir = 0;
            dir = E_Direction::INDEFINITE;
            return;
            break;
            //
        }
        return;
    }
    //
    //
    //
    if(ship_find == 1 && found_dir == 1)
    {
        bool while1 = 1;
        while(while1)
        {
            switch(dir)
            {
            case 0: y_to_attac = y_last_attac + 1; break;
            case 1: x_to_attac = x_last_attac + 1; break;
            case 2: y_to_attac = y_last_attac - 1; break;
            case 3: x_to_attac = x_last_attac - 1; break;
            }
            //
            if(x_to_attac >= 0 && x_to_attac < BOARD_SIZE && y_to_attac >=0 && y_to_attac < BOARD_SIZE)
            {
                if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF3 - P1: " << x_to_attac << " " << y_to_attac << endl;
                if(CONSOLE_DEBUG)getch();
                if(board_enemy[x_to_attac][y_to_attac] == E_Board_field::EMPTY)
                {
                    if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF3 - P2: " << x_to_attac << " " << y_to_attac << endl;
                    if(CONSOLE_DEBUG)getch();
                    ans = AttackOnOpponent(x_to_attac,y_to_attac,opponent);
                    //
                    switch(ans)
                    {
                    case 0:
                        if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF3 - CASE 0: " << x_to_attac << " " << y_to_attac << endl;
                        if(CONSOLE_DEBUG)getch();
                        board_enemy[x_to_attac][y_to_attac] = E_Board_field::MISS;
                        opponent->board_my[x_to_attac][y_to_attac] = E_Board_field::ATTACED_BY_ENEMY;
                        //
                        switch(dir)
                        {
                        case NORTH: dir = E_Direction::SOUTH; break;
                        case EAST: dir = E_Direction::WEST; break;
                        case SOUTH: dir = E_Direction::NORTH; break;
                        case WEST: dir = E_Direction::EAST; break;
                        }
                        //
                        x_last_attac = x_first;
                        y_last_attac = y_first;
                        //
                        while1=0;
                        return;
                        break;
                        //
                    case 1:
                        if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF3 - CASE 1: " << x_to_attac << " " << y_to_attac << endl;
                        if(CONSOLE_DEBUG)getch();
                        board_enemy[x_to_attac][y_to_attac] = E_Board_field::HIT;
                        x_last_attac = x_to_attac;
                        y_last_attac = y_to_attac;
                        attaced_ship_size++;
                        while1=0;
                        return;
                        break;
                        //
                    case 2:
                        if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF3 - CASE 2: " << x_to_attac << " " << y_to_attac << endl;
                        if(CONSOLE_DEBUG)getch();
                        attaced_ship_size++;
                        //
                        switch(dir)
                        {
                        case NORTH: dir = E_Direction::SOUTH; break;
                        case EAST: dir = E_Direction::WEST; break;
                        case SOUTH: dir = E_Direction::NORTH; break;
                        case WEST: dir = E_Direction::EAST; break;
                        }
                        //
                    {
                        cShip provisional_obj(attaced_ship_size,x_to_attac,y_to_attac,dir);
                        SetShipOnBoardENEMY(provisional_obj);
                    }
                        //
                        attaced_ship_size = 0;
                        ship_find = 0;
                        found_dir = 0;
                        s_dir = 0;
                        dir = E_Direction::NORTH;
                        while1=0;
                        return;
                        break;
                        //
                    }
                }
                else
                {
                    if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF3 - P3: " << x_to_attac << " " << y_to_attac << endl;
                    if(CONSOLE_DEBUG)getch();
                    switch(dir)
                    {
                    case NORTH: dir = E_Direction::SOUTH; break;
                    case EAST: dir = E_Direction::WEST; break;
                    case SOUTH: dir = E_Direction::NORTH; break;
                    case WEST: dir = E_Direction::EAST; break;
                    }
                    //
                    x_last_attac = x_first;
                    y_last_attac = y_first;
                }
            }
            else
            {
                if(CONSOLE_DEBUG) cout << "FUN - BootAI - IF3 - P4: " << x_to_attac << " " << y_to_attac << endl;
                if(CONSOLE_DEBUG)getch();
                switch(dir)
                {
                case NORTH: dir = E_Direction::SOUTH; break;
                case EAST: dir = E_Direction::WEST; break;
                case SOUTH: dir = E_Direction::NORTH; break;
                case WEST: dir = E_Direction::EAST; break;
                }
                //
                x_last_attac = x_first;
                y_last_attac = y_first;
            }
        }
        return;
    }

}



































































