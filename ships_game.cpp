#include "ships_game.h"

void PLAY(cBoard Player1, cBoard Player2) //OK
{
    Clear();
    //
    g_turn = 'A';
    cBoard * Player;
    cBoard * Oponent;
    //
    bool GameEnd = 0;
    while(!GameEnd)
    {
        if(g_turn == 'A')
        {
            Player = &Player1;
            Oponent = &Player2;
        }
        else
        {
            Player = &Player2;
            Oponent = &Player1;
        }
        //
        //
        //
        bool StillAllive = 0;
        if(Player->BOOT == true) // Jeżeli jest kolej przeciwnika i to jego kolej do ataku. (Player->BOOT == 1)
        {
            Player->BootAI(Oponent);
            //
            StillAllive = 0;
            for(int i = 0; i < (int)(Oponent->vships.size()) ; i++)
            {
                if(Oponent->vships[i].hp > 0 ) StillAllive = 1;
            }
            if(!StillAllive)
            {
                GameEnd = 1;
                cout << "KOMPUTER WYGRAL " << endl;
                getch();
                break;
            }
            if(!GameEnd)
            {
                if(g_turn == 'A')   g_turn = 'B';
                else g_turn = 'A';
            }
        }
        else
        {
            while(1)
            {
                short x_to_attack, y_to_attack;
                while(1)
                {
                    Clear();
                    cout << "GRACZ: " << Player->name << endl;
                    Player->DrawDebugBoard();
                    //
                    cout << endl;
                    cout << "PODAJ PUNKT DO ATAKU: ";
                    string to_attack;
                    cin >> to_attack;
                    tuple <bool,int,int> ans;
                    ans = FromS1toS2(to_attack);
                    if((int)get<0>(ans) == 1)
                    {
                        x_to_attack = get<1>(ans);
                        y_to_attack = get<2>(ans);
                        break;
                    }
                    else
                    {
                        cout << endl;
                        cout << "Niewlasciwe wspolrzedne " << endl;
                        getch();
                    }
                }
                //
                if(Player->board_enemy[x_to_attack][y_to_attack] == E_Board_field::EMPTY)
                {
                    int result = Player->AttackOnOpponent(x_to_attack,y_to_attack,Oponent);
                    //
                    Clear();
                    cout << "GRACZ: " << Player->name << endl;
                    Player->DrawDebugBoard();
                    //
                    switch(result)
                    {
                    case 0:
                        cout << "PUDLO" << endl;
                        break;
                        //
                    case 1:
                        cout << "TRAFIONY" << endl;
                        break;
                        //
                    case 2:
                        cout << "TRAFIONY ZATOPIONY" << endl;
                        break;
                        //
                    }
                    //
                    StillAllive = 0;
                    for(int i = 0; i < (int)(Oponent->vships.size()) ; i++)
                    {
                        if(Oponent->vships[i].hp > 0 ) StillAllive = 1;
                    }
                    if(!StillAllive)
                    {
                        GameEnd = 1;
                        break;
                    }
                    //
                    int time_in = time(NULL);
                    int time_out = time(NULL);
                    int time_last = time(NULL) - 1;
                    if(!(Oponent->BOOT == 1))
                    {
                        while(!(time_out-time_in >= DELAY_TIME))
                        {
                            if(time_out - time_last >= 1)
                            {
                                Clear();
                                cout << "ZMIANA GRACZA ZA: "  << DELAY_TIME-(time_out-time_in) << endl;
                                time_last = time(NULL);
                            }
                            time_out = time(NULL);
                        }
                    }
                    //
                    if(!GameEnd)
                    {
                        if(g_turn == 'A')g_turn = 'B';
                        else g_turn = 'A';
                    }
                    //
                    break;
                }
            }
        }
    }
    Clear();
    cout << "WYGRAL GRACZ " << Player->name << endl;
    cout << endl << endl;;
    Player->DrawDebugBoard();
    cout << endl;
    Oponent->DrawDebugBoard();
    getch();
    getch();
    getch();
    //
    cout << "KONIEC GRY - ZWYCIEZCA GRACZ: " <<  Player->name << endl;
}

void LogoDraw()
{
    string banner =
    {
        "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        "oooooo~~~oooooo~~~~~oooo~~~~~~~~~~~~~~~~~~~~~~~~.oooooo..o~oooo~~~~~~~~~o8o~~~~~~~~~~~~~~~~~~~~~~~"
        "~`888.~~~~`888.~~~~~.8'~~~~~~~~~~~~~~~~~~~~~~~~d8P'~~~~`Y8~`888~~~~~~~~~`\"'~~~~~~~~~~~~~~~~~~~~~~~"
        "~~`888.~~~.8888.~~~.8'~~~~.oooo.~~~oooo~d8b~~~~Y88bo.~~~~~~~888~.oo.~~~oooo~~oo.ooooo.~~~.oooo.o~~"
        "~~~`888~~.8'`888.~.8'~~~~`P~~)88b~~`888\"\"8P~~~~~`\"Y8888o.~~~888P\"Y88b~~`888~~~888'~`88b~d88(~~\"8~~"
        "~~~~`888.8'~~`888.8'~~~~~~.oP\"888~~~888~~~~~~~~~~~~~`\"Y88b~~888~~~888~~~888~~~888~~~888~`\"Y88b.~~~"
        "~~~~~`888'~~~~`888'~~~~~~d8(~~888~~~888~~~~~~~~oo~~~~~.d8P~~888~~~888~~~888~~~888~~~888~o.~~)88b~~"
        "~~~~~~`8'~~~~~~`8'~~~~~~~`Y888\"\"8o~d888b~~~~~~~8\"\"88888P'~~o888o~o888o~o888o~~888bod8P'~8""888P'~~"
        "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~888~~~~~~~~~~~~~~~~~"
        "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~o888o~~~~~~~~~~~~~~~~~~"
        "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    };
    //
    SetConsoleTextAttribute( hOut, 15 );

    hOut = GetStdHandle( STD_OUTPUT_HANDLE );
    int space = 97;
    for(int i=0; i<(int)banner.length(); i++)
    {
        if(banner[i] == '~')
        {
            SetConsoleTextAttribute( hOut, 3);  //tlo
        }
        else
        {
            SetConsoleTextAttribute( hOut, 15 | FOREGROUND_INTENSITY);  //napis
        }

        cout << banner[i];
        if(i==space)
        {
            cout << endl;
            space+=98;
        }
    }
    space=97;
    cout << endl << endl;
    SetConsoleTextAttribute( hOut, 15 );
}

void MENU() //OK
{
    struct
    {
        short gamemode;
        //
        short Destroyer_units;
        short Submarine_units;
        short Cruiser_units;
        short Battleship_units;
        short Carrier_units;
    }PreparedGameSettings;
    //
    bool while1 = 1;
    //
    PreparedGameSettings.gamemode = 0;
    PreparedGameSettings.Destroyer_units = 1;
    PreparedGameSettings.Submarine_units = 1;
    PreparedGameSettings.Cruiser_units = 2;
    PreparedGameSettings.Battleship_units = 3;
    PreparedGameSettings.Carrier_units = 4;
    //
    while(while1)
    {
        Clear();
        LogoDraw();
        //
        cout << "Menu: " << endl;
        cout << "1. Tryb gry." << endl;
        cout << "2. Ustawienia gry." << endl;
        cout << "3. Rozpocznik gre." << endl;
        cout << "---> ";
        string choice_str;
        cin >> choice_str;
        int choice_int = stringtoint(choice_str);
        //
        switch(choice_int)
        {
        case 1:
        {
            Clear();
            LogoDraw();
            //
            cout << "Tryb gry: " << endl;
            cout << "1. Gracz vs Komputer." << endl;
            cout << "2. Gracz vs Gracz." << endl;
            cout << "---> ";
            //
            string choice_str2;
            cin >> choice_str2;
            int choice_int2 = stringtoint(choice_str2);
            if(choice_int2 == 1)
            {
                PreparedGameSettings.gamemode = 0;
            }
            else if(choice_int2 == 2)
            {
                PreparedGameSettings.gamemode = 1;
            }
            //
            break;
            //
        }
        case 2:
        {
            Clear();
            LogoDraw();
            //
            cout << "Ustawienia gry: " << endl;
            cout << "1. Standardowe." << endl;
            cout << "2. Nie standardowe (tylko dla Gracz vs Gracz)." << endl;
            cout << "---> ";
            //
            string choice_str3;
            cin >> choice_str3;
            int choice_int3 = stringtoint(choice_str3);
            if(choice_int3 == 1)
            {
                PreparedGameSettings.Destroyer_units = 1;
                PreparedGameSettings.Submarine_units = 1;
                PreparedGameSettings.Cruiser_units = 2;
                PreparedGameSettings.Battleship_units = 3;
                PreparedGameSettings.Carrier_units = 4;
            }
            else
            {
                while(1)
                {
                    cout << "Podaj ilość statkow wielkosci 5: ";
                    string choice_str4_5;
                    cin >> choice_str4_5;
                    int ans5 = stringtoint(choice_str4_5);
                    if(ans5 != 0)
                    {
                        PreparedGameSettings.Destroyer_units = ans5;
                        break;
                    }
                }
                //
                while(1)
                {
                    cout << "Podaj ilość statkow wielkosci 4: ";
                    string choice_str4_4;
                    cin >> choice_str4_4;
                    int ans4 = stringtoint(choice_str4_4);
                    if(ans4 != 0)
                    {
                        PreparedGameSettings.Submarine_units = ans4;
                        break;
                    }
                }
                //
                while(1)
                {
                    cout << "Podaj ilość statkow wielkosci 3: ";
                    string choice_str4_3;
                    cin >> choice_str4_3;
                    int ans3 = stringtoint(choice_str4_3);
                    if(ans3 != 0)
                    {
                        PreparedGameSettings.Cruiser_units = ans3;
                        break;
                    }
                }
                //
                while(1)
                {
                    cout << "Podaj ilość statkow wielkosci 2: ";
                    string choice_str4_2;
                    cin >> choice_str4_2;
                    int ans2 = stringtoint(choice_str4_2);
                    if(ans2 != 0)
                    {
                        PreparedGameSettings.Battleship_units = ans2;
                        break;
                    }
                }
                while(1)
                {
                    cout << "Podaj ilość statkow wielkosci 1: ";
                    string choice_str4_1;
                    cin >> choice_str4_1;
                    int ans1 = stringtoint(choice_str4_1);
                    if(ans1 != 0)
                    {
                        PreparedGameSettings.Carrier_units = ans1;
                        break;
                    }
                }
            }
            break;
            //
        }
        case 3:
        {
            if(PreparedGameSettings.gamemode == 0)
            {
                GameSettings.gamemode = 0;
                GameSettings.Destroyer_units = 1;
                GameSettings.Submarine_units = 1;
                GameSettings.Cruiser_units = 2;
                GameSettings.Battleship_units = 3;
                GameSettings.Carrier_units = 4;
            }
            else if(PreparedGameSettings.gamemode == 1)
            {
                GameSettings.gamemode = 1;
                GameSettings.Destroyer_units = PreparedGameSettings.Destroyer_units;
                GameSettings.Submarine_units = PreparedGameSettings.Submarine_units;
                GameSettings.Cruiser_units = PreparedGameSettings.Cruiser_units;
                GameSettings.Battleship_units = PreparedGameSettings.Battleship_units;
                GameSettings.Carrier_units = PreparedGameSettings.Carrier_units;
            }
            while1 = 0;
        }
            break;
            //
        }
    }
}
