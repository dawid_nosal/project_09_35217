#include "cship.h"

cShip::cShip(short _size, short _position_x , short _position_y, E_Direction _direction)
{
    size = _size;
    hp = _size;
    position_x = _position_x;
    position_y = _position_y;
    direction = _direction;
}

cShip::~cShip()
{

}

int cShip::Ship_hited(int _xpos, int _ypos)
{
    bool hitted = 0;

    switch(direction)
    {
    case NORTH:
        for(int i = position_y; i <= position_y+(size-1); i++)
        {
            if(_xpos == position_x && _ypos == i) hitted = 1;
        }
        break;
        //
    case EAST:
        for(int i = position_x; i <= position_x+(size-1); i++)
        {
            if(_xpos == i && _ypos == position_y) hitted = 1;
        }
        break;
        //
    case SOUTH:
        for(int i = position_y; i >= position_y-(size-1); i--)
        {
            if(_xpos == position_x && _ypos == i) hitted = 1;
        }
        break;
        //
    case WEST:
        for(int i = position_x; i >= position_x-(size-1); i--)
        {
            if(_xpos == i && _ypos == position_y) hitted = 1;
        }
        break;
        //
    }
    //
    if(hitted)
    {
        hp--;
        //
        if(hp <= 0) return 2;
        else return 1;
    }
    else return 0;
}
