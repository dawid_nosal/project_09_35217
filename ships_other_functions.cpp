#include "ships_other_functions.h"

void Clear()
{
#if defined _WIN32
    system("cls");
    //clrscr(); // including header file : conio.h
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
    //std::cout<< u8"\033[2J\033[1;1H"; //Using ANSI Escape Sequences
#elif defined (__APPLE__)
    system("clear");
#endif
}

void AddToships_to_initiate(int type, int quantity) //OK
{
    for(int i = 0; i < quantity; i++)
    {
        g_ship_to_initiate.push(type);
    }
}
//
void SetGameSettings() // Ok
{
    AddToships_to_initiate(5,GameSettings.Destroyer_units);
    AddToships_to_initiate(4,GameSettings.Submarine_units);
    AddToships_to_initiate(3,GameSettings.Cruiser_units);
    AddToships_to_initiate(2,GameSettings.Battleship_units);
    AddToships_to_initiate(1,GameSettings.Carrier_units);
}
//
tuple <bool,int,int> FromS1toS2(string _str) //OK
{
    short length = _str.length();
    //
    if(length != 2 && length != 3)
    {
        if(CONSOLE_DEBUG) cout << "FUN - FromS1toS2: EXP 1" << endl;
        if(CONSOLE_DEBUG) cout << "LENGTH: " << length << endl;
        return {0,0,0};
    }
    //
    transform(_str.begin(),_str.end(),_str.begin(),::toupper);
    //
    char character1;
    character1 = _str[0];
    //
    int x_position;
    if(character1 >= 'A' && character1 <= 'J')
    {
        x_position = (int)character1 - 65;
    }
    else
    {
        if(CONSOLE_DEBUG) cout << "FUN - FromS1toS2: EXP 2" << endl;
        return {0,0,0};
    }
    //
    char character2[2];
    if(length == 2)
    {
        character2[0] = '0';
        character2[1] = _str[1];
    }
    else
    {
        character2[0] = _str[1];
        character2[1] = _str[2];
    }
    //
    int y_position , character2_int;
    character2_int = atoi(character2);
    if(character2_int >= 1 && character2_int <= 10)
    {
        y_position = BOARD_SIZE - character2_int;
    }
    else
    {
        if(CONSOLE_DEBUG) cout << "FUN - FromS1toS2: EXP 3" << endl;
        if(CONSOLE_DEBUG) cout << "character2_int: " << character2_int << endl;
        return {0,0,0};
    }
    //
    return {1,x_position,y_position};
}
//
string FromS2toS1(int _x, int _y) //OK
{
    char x = _x + 65;
    short y = BOARD_SIZE - _y + 48;
    //
    string ans = "";
    ans += x;
    ans += y;
    //
    return ans;
}
//
int stringtoint(string _str) //OK
{
    short length = _str.length();
    bool isnumber = 1;
    //
    for(int i = 0; i < length ; i++)
    {
        if(!(_str[i] >= '0' && _str[i] <= '9'))
        {
            isnumber = 0;
            if(CONSOLE_DEBUG) cout << "FUN - stringtoint: EXP 1" << endl;
        }
    }
    //
    int ans;
    if(isnumber == 1)
    {
        ans = stoi(_str);
        return ans;
    }
    else return 0;
}
//
char stringtochar(string _str) //OK
{
    char character = _str[0];
    return character;
}
