#ifndef SHIPS_ENUMS_H
#define SHIPS_ENUMS_H

enum E_Direction
{
    INDEFINITE = -1,
    NORTH = 0,
    EAST,
    SOUTH,
    WEST
};

enum E_Board_field
{
    EMPTY = 0,
    SHIP_ALIVE,
    SHIP_DESTROY,
    WATER_BY_SHIP,
    HIT,
    MISS,
    ATTACED_BY_ENEMY
};

#endif // SHIPS_ENUMS_H
