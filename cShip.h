#ifndef CSHIP_H
#define CSHIP_H

#include "ships_enums.h"

class cShip
{
public:
public:
    short size, position_x, position_y;
    E_Direction direction;
    short hp;

    cShip(short _size, short _position_x , short _position_y, E_Direction _direction);

    ~cShip();

    int Ship_hited(int _xpos, int _ypos);
};

#endif // CSHIP_H
